package main

import (
	"flag"

	"gitlab.com/akita/gcn3/samples/runner"
	"gitlab.com/syifan/multi_gpu_benchmarks/benchmarks/amdappsdk/matrixtranspose"
)

var dataWidth = flag.Int("width", 256, "The dimension of the square matrix.")

func main() {
	flag.Parse()

	runner := runner.Runner{}
	runner.Init()

	benchmark := matrixtranspose.NewBenchmark(runner.GPUDriver)
	benchmark.Width = *dataWidth
	runner.Benchmark = benchmark

	runner.Run()
}

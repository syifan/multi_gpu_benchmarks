module gitlab.com/syifan/multi_gpu_benchmarks

require (
	gitlab.com/akita/akita v1.0.1
	gitlab.com/akita/gcn3 v1.0.2
	golang.org/x/tools v0.0.0-20190212195815-340a1cdb5070 // indirect
)
